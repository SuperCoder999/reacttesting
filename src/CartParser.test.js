import CartParser from './CartParser';
import path from 'path';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	it('should return file\'s contents as json', () => {
		parser.readFile = jest.fn(() => `
Product name,Price,Quantity
Tvoluptatem,10.32,1`);

		parser.validate = jest.fn(() => []);
		parser.calcTotal = jest.fn(() => 10);
		parser.parseLine = jest.fn(() => ({ name: 'Fake', price: 10, quantity: 1 }));

		const json = parser.parse(path.join(__dirname, 'samples', 'cart.csv'));

		expect(parser.parseLine).toBeCalledWith('Tvoluptatem,10.32,1');

		expect(json).toMatchObject({
			items: [
				{
					name: 'Fake',
					price: 10,
					quantity: 1
				},
			],
			total: 10
		});
	});

	it('should fail when validation is not successful', () => {
		parser.readFile = jest.fn(() => `
Product name,Price
Tvoluptatem,10.32,1`);

		parser.validate = jest.fn(() => [{}]);
		parser.calcTotal = jest.fn(() => 10);
		parser.parseLine = jest.fn(() => ({ name: 'Fake', price: 10, quantity: 1 }));

		expect(() => parser.parse(path.join(__dirname, 'samples', 'cart.csv'))).toThrow();
		expect(parser.calcTotal).not.toBeCalled();
	});
	
	it('should not return error, when contents are valid', () => {
		expect(parser.validate(`
Product name,Price,Quantity
Tvoluptatem,10.32,1`).length).toBe(0);
	});

	it('should return one HEADER error, when headers are invalid', () => {
		const errors = parser.validate(`
Productname,Price,Quantity
Tvoluptatem,10.32,1`)

		expect(errors.length).toBe(1);
		expect(errors[0].type).toBe(parser.ErrorType.HEADER);
	});

	it('should return one ROW error, when number of cells is invalid', () => {
		const errors = parser.validate(`
Product name,Price,Quantity
Tvoluptatem,10.32`);

		expect(errors.length).toBe(1);
		expect(errors[0].type).toBe(parser.ErrorType.ROW);
	});

	it('should return one CELL error, when cell type is invalid', () => {
		const errors = parser.validate(`
Product name,Price,Quantity
Tvoluptatem,-10.32,1`);

		expect(errors.length).toBe(1);
		expect(errors[0].type).toBe(parser.ErrorType.CELL);
	});

	it('should parse valid CSV line', () => {
		const line = `Tvoluptatem,10.32,1`;

		expect(parser.parseLine(line)).toMatchObject({
			name: 'Tvoluptatem',
			price: 10.32,
			quantity: 1
		});
	});

	it('should calculate total price correctly, when quantity is 1', () => {
		expect(parser.calcTotal([
			{
				price: 10,
				quantity: 1
			}
		])).toBe(10);
	});

	it('should calculate total price correctly, when quantity is 5', () => {
		expect(parser.calcTotal([
			{
				price: 10,
				quantity: 5
			}
		])).toBe(50);
	});

	it('should calculate multiple items\' total price correctly with quantity 1', () => {
		expect(parser.calcTotal([
			{
				price: 10,
				quantity: 1
			},
			{
				price: 7,
				quantity: 1
			}
		])).toBe(17);
	});

	it('should calculate multiple items\' total price correctly with different quantity', () => {
		expect(parser.calcTotal([
			{
				price: 10,
				quantity: 6
			},
			{
				price: 7,
				quantity: 10
			}
		])).toBe(130);
	});
});

describe('CartParser - integration test', () => {
	it('should parse multiple lines correctly', () => {
		parser.readFile = jest.fn(() => `
Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1
Scelerisque2 lacinia,18.90,12
Consectetur2 adipiscing,29.72,10`);

		const result = parser.parse();

		expect(result.items[2]).toMatchObject({
			name: 'Scelerisque lacinia',
			price: 18.9,
			quantity: 1
		});
	});
});
